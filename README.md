# homelab-accelerator


copy/rename the compose/.env.template file, and replace with actual proper values

SQL file can be used to create the filerun and photoprism user/database in the MariaDb via phpmyadmin

traefik-desktop-docker-compose.yml - intended for Docker Desktop, local testing, no SSL or certs
  - access via http://traefik.docker.localhost

traefik-docker-compose.yml-template - intended for a public proxy, with Google OAuth 2.0 auth for all sites
  - access via 
    - https://traefik.yourdomain
    - https://traefik.yourseconddomain
    - ensure *.yourdomain redirects to the A record of the website in DNS
