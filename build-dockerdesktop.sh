#!/bin/bash

touch traefik/acme.json

# Build the Docker Containers under docker desktop
# COMPOSE_IGNORE_ORPHANS=1 docker-compose -f desktop-docker-compose.yml up -d -t 300
for f in compose/*-docker-compose.yml ; do COMPOSE_IGNORE_ORPHANS=1 docker-compose -f $f up -d -t 300 ; done
