#!/bin/bash

# Init a non tracked file
touch traefik/acme.json

# Ensure DSM is not using 80 or 443, even if updated
HTTP_PORT=81
HTTPS_PORT=444
sed -i "s/^\([ \t]\+listen[ \t]\+[]:[]*\)80\([^0-9]\)/\1$HTTP_PORT\2/" /usr/syno/share/nginx/*.mustache
sed -i "s/^\([ \t]\+listen[ \t]\+[]:[]*\)443\([^0-9]\)/\1$HTTPS_PORT\2/" /usr/syno/share/nginx/*.mustache
systemctl restart nginx

# Build the Docker Containers
for f in compose/*-docker-compose.yml ; do COMPOSE_IGNORE_ORPHANS=1 docker-compose -f $f up -d -t 300 ; done
